<?php
 
/*
 * Following code will get single match details
 * A match is identified by id 
 */
 
// array for JSON response
$response = array();
 
$id = $_POST['ID'];

$user = "root";  
$password = "";  
$host ="localhost:8080";  
$db_name ="matchs_android";  
$con = mysqli_connect($host,$user,$password,$db_name); 
 
$result = mysql_query("SELECT * FROM match WHERE ID = '$id'"); 
 
    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {
 
            $result = mysql_fetch_array($result);
 
            $match = array();
            $match["id"] = $result["ID"];
            $match["date"] = $result["Date"];
            $match["j1"] = $result["Joueur1"];
            $match["j2"] = $result["Joueur2"];
            $match["pts1"] = $result["Pts_J1"];
            $match["pts2"] = $result["Pts_J2"];
            // success
            $response["success"] = 1;
 
            // user node
            $response["match"] = array();
 
            array_push($response["match"], $match);
 
            // echoing JSON response
            echo json_encode($response);
        } else {
            // no match found
            $response["success"] = 0;
            $response["message"] = "No match found";
 
            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no match found
        $response["success"] = 0;
        $response["message"] = "No match found";
 
        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Vous devez entrer l'ID";
 
    // echoing JSON response
    echo json_encode($response);
}
?>