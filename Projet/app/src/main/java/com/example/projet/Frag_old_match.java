package com.example.projet;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class Frag_old_match extends Fragment {
    //ici on va avoir besoin de récupérer les infos des matchs dans des Strings et de les afficher dans les EditText
    private FragmentActivity mListener;
    private static String date, j1, j2, pts1, pts2;
    private String Date, J1, J2, Pts1, Pts2;
    private EditText affDate, affJ1, affJ2, affPts1, affPts2;

    public Frag_old_match() {

    }

    //on récupère les infos envoyées en paramètres
    public static Frag_old_match displayMatch(String _date, String _j1, String _j2, String _pts1, String _pts2) {
        //Création du fragment
        Frag_old_match fragment = new Frag_old_match();
        Bundle args = new Bundle();
        //récupération des infos dans le Bundle
        args.putString(date, _date);
        args.putString(j1, _j1);
        args.putString(j2, _j2);
        args.putString(pts1, _pts1);
        args.putString(pts2, _pts2);
        //insertion du Bundle dans le fragment
        fragment.setArguments(args);
        return fragment;
    }

    //création du fragment
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Date = getArguments().getString(date);
            J1 = getArguments().getString(j1);
            J2 = getArguments().getString(j2);
            Pts1 = getArguments().getString(pts1);
            Pts2 = getArguments().getString(pts2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //création de la vue avec le layout correspondant
        View view = inflater.inflate(R.layout.frag_old_match, container,false);

        //affichage des EditText avec les infos récupérées
        affDate = view.findViewById(R.id.date);
        affDate.setText(Date);
        affJ1 = view.findViewById(R.id.j1);
        affJ1.setText(J1);
        affJ2 = view.findViewById(R.id.j2);
        affJ2.setText(J2);
        affPts1 = view.findViewById(R.id.pts1);
        affPts1.setText(Pts1);
        affPts2 = view.findViewById(R.id.pts2);
        affPts2.setText(Pts2);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AnciensMatchs) {
            mListener = (FragmentActivity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach(){
        super.onDetach();
        mListener = null;
    }
}
