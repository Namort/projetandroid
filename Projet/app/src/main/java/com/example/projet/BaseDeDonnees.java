package com.example.projet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;

import static android.content.ContentValues.TAG;

public class BaseDeDonnees extends SQLiteOpenHelper {

    //Strings qui vont servir pour créer la database et les requetes

    private static final String DATABASE_NAME = "MATCH";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_MATCH = "Matchs";

    private static final String KEY_MATCH_ID = "ID";
    private static final String KEY_MATCH_DATE = "Date";
    private static final String KEY_MATCH_J1 = "Joueur1";
    private static final String KEY_MATCH_J2 = "Joueur2";
    private static final String KEY_MATCH_PTS_1 = "Pts_1";
    private static final String KEY_MATCH_PTS_2 = "Pts_2";

    public BaseDeDonnees(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {

        //création de la table match : date du match, lieu, noms des joueurs et leurs points
        String CREATE_MATCH_TABLE = "CREATE TABLE " + TABLE_MATCH +
                "(" +
                    KEY_MATCH_ID + " INTEGER PRIMARY KEY," +
                    KEY_MATCH_DATE + " DATE," +
                    KEY_MATCH_PTS_1 + "INT, " +
                    KEY_MATCH_PTS_2 + "INT, " +
                    KEY_MATCH_J1 + "STRING," +
                    KEY_MATCH_J2 + "STRING" +
                ")";

        db.execSQL(CREATE_MATCH_TABLE);
    }



    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MATCH);

            onCreate(db);
        }
    }

    //insertion d'un match dans la table
    public void addMatch(String date, String j1, String j2, int pts1, int pts2 ) {
        //on ouvre la bdd en écriture
        SQLiteDatabase bdd = getWritableDatabase();
        bdd.beginTransaction();

            try {
                ContentValues values = new ContentValues();
                //insertion des infos du match
                values.put(KEY_MATCH_DATE, date);
                values.put(KEY_MATCH_J1, j1);
                values.put(KEY_MATCH_J2, j2);
                values.put(KEY_MATCH_PTS_1, pts1);
                values.put(KEY_MATCH_PTS_1, pts2);

                bdd.insertOrThrow(TABLE_MATCH, null, values);
                bdd.setTransactionSuccessful();

            } catch (Exception e) {
                Log.d(TAG, "Erreur lors de l'insertion dans la base de données");
            } finally {
                bdd.endTransaction();
            }

        if(getMatchNumber() > 5) {
            deleteFirstMatch(getMinID());
        }

    }

    //supprime un match dans la table
    public void deleteFirstMatch(Integer id) {
        SQLiteDatabase bdd = getWritableDatabase();
        bdd.beginTransaction();

        try {
            String clause = KEY_MATCH_ID + " = " + id;
            bdd.delete(TABLE_MATCH, clause, null);
        }
        catch (Exception e) {
            Log.d(TAG, "Error while trying to delete the match");
        } finally {
            bdd.endTransaction();
        }
    }

    //récupération du nombre de matchs enregistrés dans la bdd
    public int getMatchNumber() {
        String requete = "SELECT * FROM " + TABLE_MATCH ;

        SQLiteDatabase bdd = getReadableDatabase();
        //création du curseur pour lire la requete
        Cursor cursor = bdd.rawQuery(requete, null);

        return cursor.getCount();
    }

    //vérifie que le match avec cet ID existe bien
    public boolean doesExist(Integer id) {
        String requete = "SELECT * FROM " + TABLE_MATCH + " WHERE " + KEY_MATCH_ID + " = " + id ;

        SQLiteDatabase bdd = getReadableDatabase();
        Cursor cursor = bdd.rawQuery(requete, null);

        //si le curseur en a trouvé un c'est que c'est bon
        if (cursor.moveToFirst()) {
            return true;
        }
        else return false;
    }

    //récupère l'info qu'on veut dans la bdd
    public String getMatchInfo(Integer id, String info) {
        String requete = "SELECT * FROM " + TABLE_MATCH + " WHERE " + KEY_MATCH_ID + " = " + id ;

        SQLiteDatabase bdd = getReadableDatabase();
        Cursor cursor = bdd.rawQuery(requete, null);

        try {
            if (cursor.moveToFirst()) {
                //retourne la valeur en string de la colonne correspondant à l'info demandée
                return cursor.getString(cursor.getColumnIndex(info));
            }
        } catch (Exception e) {
            Log.d(TAG, "Erreur lors de la récupération de la donnée");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close(); //ferme le curseur
            }
        }

        return " ";
    }

    //retourne l'ID du plus petit ID
    public Integer getMinID() {
        String requete = "SELECT MIN(" + KEY_MATCH_ID + ") FROM " + TABLE_MATCH;

        SQLiteDatabase bdd = getReadableDatabase();
        Cursor cursor = bdd.rawQuery(requete, null);

        try {
            if(cursor.moveToFirst()) {
                return cursor.getInt(cursor.getColumnIndex(KEY_MATCH_ID));
            }
        }
        catch (Exception e) {
            Log.d(TAG, "Erreur lors de la récupération de l'id");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return 0;
    }

}
