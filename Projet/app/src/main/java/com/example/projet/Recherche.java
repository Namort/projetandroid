package com.example.projet;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;
import java.util.HashMap;
import java.util.Map;

public class Recherche extends AppCompatActivity {

    public EditText search;
    public String server_url = "http://localhost:8080/ProjetAndroid/read_match_id.php";
    public AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherche);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        search = (EditText) findViewById(R.id.searchbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.back:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            case R.id.photo:
                Intent intentP = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentP,0);
                return true;

            case R.id.map:
                Intent intentM = new Intent(this, Geolocalisation.class);
                startActivity(intentM);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onSearch(View view) {
        final String id = search.getText().toString();
        JsonArrayRequest arrayRequest = new JsonArrayRequest(server_url, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jresponse = jsonArray.getJSONObject(i);
                        //on récupère toutes les infos
                        String date = jresponse.getString("date");
                        String joueur1 = jresponse.getString("j1");
                        String joueur2 = jresponse.getString("j2");
                        String points1 = jresponse.getString("pts1");
                        String points2 = jresponse.getString("pts2");


                        Frag_old_match oldMatch = Frag_old_match.displayMatch(date, joueur1, joueur2, points1, points2);
                        getSupportFragmentManager().beginTransaction().add(R.id.searchres, oldMatch).commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Recherche.this, "some error found .....", Toast.LENGTH_SHORT).show();
                error.printStackTrace();

            }
        })


        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map <String,String> Params = new HashMap<String, String>();
                Params.put("ID",id);
                return Params;
            }
        };
        Mysingleton.getInstance(Recherche.this).addTorequestque(arrayRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "photo","photo");
    }
}
