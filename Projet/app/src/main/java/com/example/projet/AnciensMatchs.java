package com.example.projet;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import static android.content.ContentValues.TAG;

public class AnciensMatchs extends AppCompatActivity {

    public BaseDeDonnees bdd ;
    public String date, joueur1, joueur2, points1, points2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_match);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bdd = new BaseDeDonnees(this);
        Integer i = bdd.getMinID();

        if(i > 0) {
            do {
                date = bdd.getMatchInfo(i, "Date");
                joueur1 = bdd.getMatchInfo(i, "Joueur 1");
                joueur2 = bdd.getMatchInfo(i, "Joueur 2");
                points1 = bdd.getMatchInfo(i, "Points du joueur 1");
                points2 = bdd.getMatchInfo(i, "Points du joueur 2");

                Frag_old_match oldMatch = Frag_old_match.displayMatch(date, joueur1, joueur2, points1, points2);
                getSupportFragmentManager().beginTransaction().add(R.id.match, oldMatch).commit();

                i++;
            } while (bdd.doesExist(i));
        }
        else {
            Log.d(TAG, "No match found");
        }
    }

    //création du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_app, menu);
        return true;
    }

    //lors du click sur une option du menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.back:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            case R.id.photo:
                Intent intentP = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentP,0);
                return true;

            case R.id.map:
                Intent intentM = new Intent(this, Geolocalisation.class);
                startActivity(intentM);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "photo","photo");
    }
}
