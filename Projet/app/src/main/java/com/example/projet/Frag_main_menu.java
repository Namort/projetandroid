package com.example.projet;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class Frag_main_menu extends Fragment {
    //on crée la vue avec le layout fragment correspondant
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("Frag_main_menu", "Banane !!");
        return inflater.inflate(R.layout.frag_main_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        view.findViewById(R.id.nouveau).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentN = new Intent(requireContext(), NouveauMatchForm.class);
                startActivity(intentN);
            }
        });

        view.findViewById(R.id.anciens).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentA = new Intent(requireContext(), AnciensMatchs.class);
                startActivity(intentA);
            }
        });
    }
}