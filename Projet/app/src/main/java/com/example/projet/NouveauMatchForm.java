package com.example.projet;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class NouveauMatchForm extends AppCompatActivity {

    public EditText joueur1, joueur2, autre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_form);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Frag_new_form newForm = new Frag_new_form();
        getSupportFragmentManager().beginTransaction().add(R.id.new_form, newForm).commit();


        joueur1 = findViewById(R.id.j1);
        joueur2 = findViewById(R.id.j2);
        autre = findViewById(R.id.autre);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.back:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            case R.id.photo:
                Intent intentP = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentP,0);
                return true;

            case R.id.map:
                Intent intentM = new Intent(this, Geolocalisation.class);
                startActivity(intentM);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //quand on clique sur le bouton "Valider"
    public void onValidate(View view) {

        Intent intent = new Intent(this, NouveauMatch.class);
        intent.putExtra("Joueur1", joueur1.getText().toString());
        intent.putExtra("Joueur2", joueur2.getText().toString());
        intent.putExtra("Autre", autre.getText().toString());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "photo","photo");
    }
}
