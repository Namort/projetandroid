package com.example.projet;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NouveauMatch extends AppCompatActivity {

    public EditText j1, j2;
    public TextView pts1, pts2;
    public String nom1, nom2;
    public int points1, points2;
    public BaseDeDonnees bdd = new BaseDeDonnees(this);

    public String server_url = "http://localhost:8080/ProjetAndroid/create_match.php";
    public AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_match);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Frag_new_match newMatch = new Frag_new_match();
        getSupportFragmentManager().beginTransaction().add(R.id.match, newMatch).commit();

        j1 = findViewById(R.id.j1);
        pts1 = findViewById(R.id.pts1);

        j2 = findViewById(R.id.j2);
        pts2 = findViewById(R.id.pts2);

        //récupère les paramètres envoyés dans l'intent
        Intent intent = getIntent();

        //affiche le nom des joueurs
        if(intent.hasExtra("Joueur1")) {
            j1.setText(intent.getStringExtra("Joueur1"));
        }

        if(intent.hasExtra("Joueur2")) {
            j2.setText(intent.getStringExtra("Joueur2"));
        }

        //initialisation des points
        points1 = 0;
        points2 = 0;

        //récupération des noms inscrits
        nom1 = j1.getText().toString();
        nom2 = j2.getText().toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.back:
                Intent intent = new Intent(this, NouveauMatchForm.class);
                startActivity(intent);
                return true;

            case R.id.photo:
                Intent intentP = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentP,0);
                return true;

            case R.id.map:
                Intent intentM = new Intent(this, Geolocalisation.class);
                startActivity(intentM);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //quand on clique sur un des boutons "+1 points"
    public void gain(View view) {
        Button point = (Button) view;

        switch (point.getId()) {

            case R.id.gain1:
                points1++;
                pts1.setText(points1);
                break;

            case R.id.gain2:
                points2++;
                pts2.setText(points2);
                break;

        }
    }

    public void onStop(View view) {

        final Date date = new Date();
        bdd.addMatch(date.toString(), nom1, nom2, points1, points2);
        builder = new AlertDialog.Builder(NouveauMatch.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                builder.setTitle("Server Response");
                builder.setMessage("Response :" + response);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(NouveauMatch.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NouveauMatch.this, "some error found .....", Toast.LENGTH_SHORT).show();
                error.printStackTrace();

            }
        })


        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map <String,String> Params = new HashMap<String, String>();
                Params.put("Date",date.toString());
                Params.put("Joueur1",nom1);
                Params.put("Joueur2",nom2);
                Params.put("Pts1",pts1.getText().toString());
                Params.put("Pts2",pts2.getText().toString());
                return Params;
            }
        };
        Mysingleton.getInstance(NouveauMatch.this).addTorequestque(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "photo","photo");
    }

}
